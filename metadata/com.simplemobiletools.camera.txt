Categories:Multimedia
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Camera
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Camera/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Camera/blob/HEAD/CHANGELOG.md

Auto Name:Simple Camera
Summary:A front/read camera with flash
Description:
This is a camera useable for simple photo taking and video recording suitable
well enough for very most people. It can switch between front and rear camera,
as well as the flash can be turned on and off. While recording a video, the
flash can be used as a flashlight. You can activate the shutter by long pressing
the preview.for easier selfies. Does not require any unnecessary permissions
needed by many other cameras.

If you want to launch this app at pressing the hardware camera button, you might
have to disable the built in Camera app in Settings -> Apps -> Camera ->
Disable.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Camera

Build:1.12,13
    commit=86848f556d30609f03aaff63562886868c6234d2
    subdir=app
    gradle=yes

Build:1.14,14
    commit=1.14
    subdir=app
    gradle=yes

Build:1.15,15
    commit=1.15
    subdir=app
    gradle=yes

Build:1.16,16
    commit=1.16
    subdir=app
    gradle=yes

Build:1.17,17
    commit=1.17
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.17
Current Version Code:17
