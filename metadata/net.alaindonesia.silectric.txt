Categories:Money
License:Apache2
Web Site:https://github.com/hoihei/Silectric/blob/HEAD/README.md
Source Code:https://github.com/hoihei/Silectric
Issue Tracker:https://github.com/hoihei/Silectric/issues

Auto Name:Silectric
Summary:Calculate electric bill
Description:
Calculate electric bill based on electric consumption in home.
.

Repo Type:git
Repo:https://github.com/hoihei/Silectric

Build:1.1.2016.8.13.1,1120168131
    commit=v1.1.2016.8.13.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.2016.8.13.1
Current Version Code:1120168131
