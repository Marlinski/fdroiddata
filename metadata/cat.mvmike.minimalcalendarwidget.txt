Categories:Theming,Time
License:BSD-3
Web Site:
Source Code:https://github.com/mvmike/min-cal-widget
Issue Tracker:https://github.com/mvmike/min-cal-widget/issues

Auto Name:MinCal Widget
Summary:Minimal month calendar widget
Description:
Calendar widget that shows the current month.

* Default 3x2 (resizable)
* Dark theme
* Displays all events from your calendars
* Pressing on header opens widget configuration
* Pressing on any day cell opens calendar application
.

Repo Type:git
Repo:https://github.com/mvmike/min-cal-widget
Binaries:https://github.com/mvmike/min-cal-widget/releases/download/v%v/min-cal-widget.apk

Build:0.0.1,1
    commit=v0.0.1
    subdir=min-cal-widget
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.0.1
Current Version Code:1
